import "./styles.css";
import { Login } from "./pages/login.component";
import init from "./mockend";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Users } from "./pages/users.component";
import { ViewUser } from "./pages/viewUser.component";
import { Books } from "./pages/books.component";
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'moment';


import React, { useEffect, useState } from "react";

export default function App() {
  const [dbReady, setdbReady] = useState(false);
  async function loadDB() {
    const db = await init();
    setdbReady(true);
  }
  useEffect(() => {
    loadDB();
  }, []);


  return (
    <div className="App">
      {dbReady ? (
        <div>
          {/*<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">*/}
          {/*  <div className="container-fluid text-center">*/}
          {/*    <div className="collapse navbar-collapse" id="navbarSupportedContent">*/}
          {/*      <ul className="navbar-nav me-auto mb-2 mb-lg-0">*/}
          {/*        <li className="nav-item">*/}
          {/*          <a className="nav-link" aria-current="page" href="users">Users</a>*/}
          {/*        </li>*/}
          {/*        <li className="nav-item">*/}
          {/*          <a className="nav-link" href="books">Books</a>*/}
          {/*        </li>*/}
          {/*      </ul>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</nav>*/}
        <Router>
          <Switch>
            <Route exact path="/users">
              <Users />
            </Route>
            <Route path="/users/:purpose/:id/">
              <ViewUser />
            </Route>
            <Route exact path="/books">
              <Books />
            </Route>
            <Route path="/">
              <Login />
            </Route>
          </Switch>
        </Router>
        </div>
      ) : (
        <>Loading DB...</>
      )}
    </div>
  );
}
