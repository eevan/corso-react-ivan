import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./reducers";
// import logger from "redux-logger";
import { ThunkAction } from "redux-thunk";
import { AnyAction } from "redux";

const store = configureStore({
  reducer: rootReducer
  // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)
});

export type AppThunk = ThunkAction<void, RootState, unknown, AnyAction>;
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

export default store;
