import React, {useState, useRef, useEffect} from "react";
import {useHistory} from "react-router-dom";
import {api} from "../mockend";

export interface IUser {
	name: string;
	surname: string;
	age: string;
	gender: string;
	height: number;
	idQuery: string;
	surnameQuery: string;
}

interface UserDTO extends Pick<IUser, "name" | "surname"> {
	_id: number;
}

export function Users() {
	const [user, setUser] = useState<IUser>({
		name: "",
		surname: "",
		age: '',
		gender: "",
		height: 0,
		idQuery: '',
		surnameQuery: ''
	});

	const [listUser, setListUser] = useState<UserDTO[]>([]);
	const history = useHistory();

	useEffect(() => {
		setTimeout(() => {
			api.users
				.getAllUsers()
				.then((list:any) => {
					setListUser(list);
					// setUsers(lista => [...lista])
				})
				.catch(console.log);
		}, 1000);
	}, []);


	function clearCreateUserForm(){
		setUser({
			name: "",
			surname: "",
			age: '',
			gender: "",
			height: 0,
			idQuery: '',
			surnameQuery: ''
		})
	}

	function onCreate(e: React.FormEvent) {
		e.preventDefault();
		// setDirty(true);
		console.log(e.target);
		// console.log({ name: formName, psw: formPsw });
		api.users
			.addUser(user)
			.then(() => api.users.getAllUsers())
			.then((response: any) => {
				setListUser(response);
				clearCreateUserForm();
			})
			.catch(console.error);
	}

	function deleteUser(id: number){
		console.log('deleting', id);
		api.users
			.deleteById({_id: id})
			.then(() => api.users.getAllUsers())
			.then((response: any) => {
				setListUser(response);
			})
			.catch(console.error);
	}

	function onView(id: number, purpose: string) {
		let path = `/users/${purpose}/${id}`;
		history.push(path);
	}

	function onChangeUser(e: React.ChangeEvent<HTMLInputElement>) {
		const {name, value} = e.currentTarget;
		console.log({name, value});
		setUser({
			...user,
			[name]: value
		});

		// setFormPsw(value);
	}

	function searchById(e: React.ChangeEvent<HTMLInputElement>) {
		const {name, value} = e.currentTarget;
		setUser({
			...user,
			[name]: +value
		});
		if(!+value){
			api.users
				.getAllUsers()
				.then((list:any) => {
					setListUser(list);
				})
				.catch(console.log);
		} else {
			api.users.getUserById({_id: +value})
				.then((response: any) => {
					setListUser([response]);
				})
		}
	}


	function searchBySurname(e: React.ChangeEvent<HTMLInputElement>) {
		const key = e.currentTarget.name;
		const value = e.currentTarget.value;
		setUser({
			...user,
			[key]: value
		});
		if(!value){
			api.users
				.getAllUsers()
				.then((list:any) => {
					setListUser(list);
				})
				.catch(console.log);
		} else {
			api.users.getUserByNameSurname({'name': value, 'surname': value})
				.then((response: any) => {
					setListUser(response);
				})
		}
	}

	return (
		<div>

			<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
				<div className="container-fluid text-center">
					<div className="collapse navbar-collapse" id="navbarSupportedContent">
						<ul className="navbar-nav me-auto mb-2 mb-lg-0">
							<li className="nav-item">
								<a className="nav-link active" aria-current="page" href="users">Users</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="books">Books</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<div className="m-5 p-4 bg-secondary bg-opacity-10 border border-dark d-flex justify-content-between">
				<div className="w-25 p-2 text-center">
					<h4 className="text-uppercase mb-5">throw in a user...</h4>
					<form className="form-group" onSubmit={onCreate}>
						<div className="d-flex flex-column align-items-center justify-content-around">
							<div>
								<div className="mb-2 d-flex align-items-center justify-content-between">
									<label htmlFor="nome">Name</label>
									<input
										id="nome"
										placeholder=""
										className="form-control-sm mx-2"
										value={user.name}
										name="name"
										type="text"
										onChange={onChangeUser}
									/>
								</div>
								<div className="mb-2 d-flex align-items-center justify-content-between">
									<label htmlFor="cognome">Surname</label>
									<input
										id="cognome"
										placeholder=""
										className="form-control-sm mx-2"
										value={user.surname}
										name="surname"
										type="text"
										onChange={onChangeUser}
									/>
								</div>
								<div className="mb-2 d-flex align-items-center justify-content-between">
									<label htmlFor="eta">Age</label>
									<input
										id="eta"
										placeholder=""
										className="form-control-sm mx-2"
										value={user.age}
										name="age"
										type="text"
										onChange={onChangeUser}
									/>
								</div>
								<div className="mb-2 d-flex align-items-center justify-content-between">
									<label htmlFor="gender">Gender</label>
									<input
										id="gender"
										placeholder=""
										className="form-control-sm mx-2"
										value={user.gender}
										name="gender"
										type="text"
										onChange={onChangeUser}
									/>
								</div>
							</div>
							<div>
								<div className="w-25 text-center">
									<button className="btn btn-primary" type="submit">
										<i className="fas fa-user-plus"></i>
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div className="w-25 p-2 text-center">
					<h4 className="text-uppercase mb-5">...Find him by ID...</h4>
					<form className="form-group">
						<div className="d-flex flex-column align-items-center justify-content-around">
							<div className="mb-2 d-flex align-items-center justify-content-between">
								<input
									id="id"
									placeholder="start typing id..."
									className="form-control-sm mx-2"
									value={user.idQuery}
									name="idQuery"
									type="text"
									onChange={searchById}
								/>
							</div>
						</div>
					</form>
				</div>
				<div className="w-25 p-2 text-center">
					<h4 className="text-uppercase mb-5">...or by his surname.</h4>
					<form className="form-group">
						<div className="d-flex flex-column align-items-center justify-content-around">
							<div className="mb-2 d-flex align-items-center justify-content-between">
								<input
									id="surname"
									placeholder="start typing surname..."
									className="form-control-sm mx-2"
									value={user.surnameQuery}
									name="surnameQuery"
									type="text"
									onChange={searchBySurname}
								/>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div className="m-5 p-4 bg-primary bg-opacity-10 border border-dark text-center">
				<h3 className="text-uppercase mb-5">...and here's your list</h3>
					<div>
						<div className="d-flex justify-content-center mb-2">
							<div className="d-flex justify-content-between align-items-center w-75">
							<h6 className="text-uppercase fw-bold w-25">Id</h6>
							<h6 className="text-uppercase fw-bold w-25">Name</h6>
							<h6 className="text-uppercase fw-bold w-25">Surname</h6>
							<h6 className="text-uppercase fw-bold w-25">actions</h6>
							</div>
						</div>
						<ul className="list-unstyled">
							{listUser.map((user) => (
								<li key={user._id} className="d-flex justify-content-center mb-2">
									<div className="d-flex justify-content-between align-items-center w-75">
										<small className="w-25">{user._id}</small>
										<small className="w-25">{user.name}</small>
										<small className="w-25"> {user.surname}</small>
										<div className="w-25">
											<button
												className="btn btn-info me-3"
												onClick={(e) => onView(user._id, 'view')} type="button">
												<i className="fas fa-eye"></i>
											</button>
											<button
												className="btn btn-warning me-3"
												onClick={(e) => onView(user._id, 'edit')} type="button">
												<i className="fas fa-user-edit"></i>
											</button>
											<button
												className="btn btn-danger"
												onClick={(e) => deleteUser(user._id)} type="button">
												<i className="fas fa-trash-alt"></i>
											</button>
										</div>
									</div>
								</li>
							))}
						</ul>
					</div>
			</div>

		</div>
	);
}
