import { useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import { api } from "../mockend";

import { Users } from "./users.component";

export function Login() {
  const [formName, setFormName] = useState<string>("");
  const [formPsw, setFormPsw] = useState<string>("");
  const history = useHistory();

  function onChangeName(e: React.ChangeEvent<HTMLInputElement>) {
    const { name, value } = e.currentTarget;
    console.log({ name, value });
  }

  function onChangePsw(e: React.ChangeEvent<HTMLInputElement>) {
    const { name, value } = e.currentTarget;
    console.log({ name, value });

    setFormPsw(value);
  }

  function onSubmit(e: React.FormEvent) {
    e.preventDefault();
    // setDirty(true);
    console.log(e.target);
    console.log({ name: formName, psw: formPsw });
    api.auth
      .login({ username: formName, password: formPsw })
      .then((response) => {
        history.push("/users");
      })
      .catch(console.log);
  }
  return (
    <div className="m-5 p-5">
      <h1>LOGIN</h1>
      <form onSubmit={onSubmit}>
        <input
          name="name"
          className="form-control-sm mx-1"
          onChange={(e) => setFormName(e.currentTarget.value)}
          placeholder="name"
          value={formName}
          type="text"
        />
        <input
          name="pws"
          className="form-control-sm mx-1"
          placeholder="psw"
          onChange={onChangePsw}
          type="text"
          value={formPsw}
        />
        <button className="btn btn-primary" type="submit">Save</button>
      </form>
    </div>
  );
}
