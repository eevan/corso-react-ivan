import React, {useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {useHistory} from "react-router-dom";
import { api } from "../mockend";
import moment from "moment";
import {IUser} from "./users.component";


interface IUserResponse {
  name: string,
  surname: string,
  age: string,
  gender: string,
  height: number,
  created: Date | null,
  _id: number,
  stringDate: string
}

export function ViewUser() {
  const params = useParams<{ id: string, purpose: string }>();

  const history = useHistory();

  const [userDetails, setUserDetails] = useState<IUserResponse>({
      name: "",
      surname: "",
      age: '',
      gender: "",
      height: 0,
      _id: -1,
      created: null,
      stringDate: ''
    }
  )

  const [user, setUser] = useState<IUser>({
    name: "",
    surname: "",
    age: '',
    gender: "",
    height: 0,
    idQuery: '',
    surnameQuery: ''
  });


  useEffect(() => {
    console.log(params.id);
    setTimeout(() => {
      api.users
        .getUserById({ '_id': +params.id })
        .then((user) => {
          setUserDetails({...user, stringDate: moment(user.created).format('HH:SS DD/MM/YYYY')});

        })
        .catch(console.log);
    });
  }, [params.id]);

  function onChangeUserDetails(e: React.ChangeEvent<HTMLInputElement>) {

    const key = e.currentTarget.name;
    const value = e.currentTarget.value;
    console.log({key, value});
    setUserDetails({
      ...userDetails,
      [key]: value
    });

    // setFormPsw(value);
  }

  function updateUser(e: React.FormEvent){
    e.preventDefault();
    console.log(e.target);
    api.users
      .updateUser({_id: userDetails._id, name: userDetails.name, surname: userDetails.surname, age: +userDetails.age, gender: userDetails.gender, height: userDetails.height})
      .then(() => backHome())
  }

  function backHome(){
    history.push('/users')
  }

  return (
    params.purpose === 'view' ?
    <div className="container m-5 w-25">
      <button onClick={backHome} className="btn btn-primary m-5">
        <i className="fas fa-arrow-circle-left mx-2 mt-2"></i>back to home
      </button>
      <div className='container m-5'>
        <h3>User</h3>
        <p><strong>Name:</strong> {userDetails.name}</p>
        <p><strong>Surname:</strong> {userDetails.surname}</p>
        <p><strong>Age:</strong> {userDetails.age}</p>
        <p><strong>Height:</strong> {userDetails.height}</p>
        <p><strong>Created:</strong> {userDetails.stringDate}</p>
      </div>
    </div> :


      <div className="container m-5 w-25">
        <button onClick={backHome} className="btn btn-primary m-5">
          <i className="fas fa-arrow-circle-left mx-2 mt-2"></i>back to home
        </button>
        <form className="form-group" onSubmit={updateUser}>
          <div className="d-flex align-items-center justify-content-between">
            <div>
              <div className="mb-2 d-flex align-items-center justify-content-between">
                <label htmlFor="nome">Name</label>
                <input
                  id="nome"
                  placeholder=''
                  className="form-control-sm mx-2"
                  value={userDetails.name}
                  name="name"
                  type="text"
                  onChange={onChangeUserDetails}
                />
              </div>
              <div className="mb-2 d-flex align-items-center justify-content-between">
                <label htmlFor="cognome">Surname</label>
                <input
                  id="cognome"
                  placeholder=''
                  className="form-control-sm mx-2"
                  value={userDetails.surname}
                  name="surname"
                  type="text"
                  onChange={onChangeUserDetails}
                />
              </div>
              <div className="mb-2 d-flex align-items-center justify-content-between">
                <label htmlFor="eta">Age</label>
                <input
                  id="eta"
                  placeholder=''
                  className="form-control-sm mx-2"
                  value={userDetails.age}
                  name="age"
                  type="text"
                  onChange={onChangeUserDetails}
                />
              </div>
              <div className="mb-2 d-flex align-items-center justify-content-between">
                <label htmlFor="gender">Gender</label>
                <input
                  id="gender"
                  placeholder=''
                  className="form-control-sm mx-2"
                  value={userDetails.gender}
                  name="gender"
                  type="text"
                  onChange={onChangeUserDetails}
                />
              </div>
            </div>
            <div>
              <div className="w-25 text-center">
                <button className="btn btn-success" type="submit">
                  <i className="fas fa-sync-alt"></i>
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
  );
}
