import React, {useState, useRef, useEffect} from "react";
import {useHistory} from "react-router-dom";
import {api} from "../mockend";
import {IUser} from "./users.component";

export interface IBook{
  name: string,
  price: number | string,
  author: string,
  isEbook: boolean,
  userId: number | string,
  idQuery: string;
  freeQuery: string
}

interface UserDTO extends Pick<IBook, "name" | "author" | "isEbook" | "price" | "userId"> {
  _id: number;
}

export function Books() {

  const [book, setBook] = useState<IBook>({
    name: '',
    price: 0,
    author: '',
    isEbook: false,
    userId: 0,
    idQuery: '',
    freeQuery: ''
  })

  useEffect(() => {
    setTimeout(() => {
      api.books
        .getAllBooks()
        .then((list:any) => {
          setListBook(list);
          // setUsers(lista => [...lista])
        })
        .catch(console.log);
    }, 1000);
  }, []);

  const [listBooks, setListBook] = useState<UserDTO[]>([]);

  function onCreate(e: React.FormEvent){
    e.preventDefault();

    api.books
      .addBook(book)
      .then(()=> api.books.getAllBooks())
      .then((response: any) => {
        setListBook(response);
        clearCreateBookForm();
      })
      .catch(console.error);
  }

  function clearCreateBookForm(){
    setBook({
      name: '',
      price: 0,
      author: '',
      isEbook: false,
      userId: 0,
      idQuery: '',
      freeQuery: ''
    })
  }

  function onChangeBook(e: React.ChangeEvent<HTMLInputElement>){
    const key = e.currentTarget.name;
    const value = e.currentTarget.value;
    setBook({
      ...book,
      [key]: value
    })
  }

  function onChangeEBook(e:any){
    setBook({
      ...book,
      isEbook: e.currentTarget.checked
    })
    console.log(book)
  }

  function deleteBook(id: number){
    api.books
      .deleteBookById({_id: id})
      .then(() => api.books.getAllBooks())
      .then((response:any) => {
        setListBook(response)
      })
      .catch(console.error)
  }

  function getBookByAuthorNameOrTitle(e: React.ChangeEvent<HTMLInputElement>){
    const key = e.currentTarget.name;
    const value = e.currentTarget.value;

    setBook({
      ...book,
      [key]: value
    })

    if(e.currentTarget.value){
      api.books
        .getBookByNameAuthor(
          {'userId': '',
            'name': e.currentTarget.value,
            'author': e.currentTarget.value})
        .then((res: any) => {
          setListBook(res)
        })
        .catch(console.error)
    } else {
      api.books
        .getAllBooks()
        .then((list:any) => {
          setListBook(list);
        })
        .catch(console.log)
    }
  }

  function getBookByUserId(e: React.ChangeEvent<HTMLInputElement>){
    const key = e.currentTarget.name;
    const value = e.currentTarget.value;
    setBook({
      ...book,
      [key]: +value
    });
    if(+value) {
      api.books
        .getBooksByUserId({'userId': value})
        .then((res: any) => {
          setListBook(res)
        })
        .catch(console.error)
    } else {
      api.books
        .getAllBooks()
        .then((list:any) => {
          setListBook(list);
        })
        .catch(console.log)
    }
  }



  return(
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div className="container-fluid text-center">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link" aria-current="page" href="users">Users</a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="books">Books</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="m-5 p-4 bg-secondary bg-opacity-10 border border-dark d-flex justify-content-between">
        <div className="w-25 p-2 text-center">
          <h4 className="text-uppercase mb-5">throw in a book...</h4>
          <form className="form-group" onSubmit={onCreate}>
            <div className="d-flex flex-column align-items-center justify-content-around">
              <div>
                <div className="mb-2 d-flex align-items-center justify-content-between">
                  <label htmlFor="userId">UserId</label>
                  <input
                    id="userId"
                    placeholder=""
                    className="form-control-sm mx-2"
                    value={book.userId ? book.userId : ''}
                    name="userId"
                    type="number"
                    onChange={onChangeBook}
                  />
                </div>
                <div className="mb-2 d-flex align-items-center justify-content-between">
                  <label htmlFor="nome">Name</label>
                  <input
                    id="nome"
                    placeholder=""
                    className="form-control-sm mx-2"
                    value={book.name}
                    name="name"
                    type="text"
                    onChange={onChangeBook}
                  />
                </div>
                <div className="mb-2 d-flex align-items-center justify-content-between">
                  <label htmlFor="prezzo">Price</label>
                  <input
                    id="prezzo"
                    placeholder=""
                    className="form-control-sm mx-2"
                    value={book.price ? book.price : ''}
                    name="price"
                    type="number"
                    onChange={onChangeBook}
                  />
                </div>
                <div className="mb-2 d-flex align-items-center justify-content-between">
                  <label htmlFor="autore">Author</label>
                  <input
                    id="autore"
                    placeholder=""
                    className="form-control-sm mx-2"
                    value={book.author}
                    name="author"
                    type="text"
                    onChange={onChangeBook}
                  />
                </div>
                <div className="mb-2 d-flex align-items-center justify-content-between">
                  <label htmlFor="ebook">Ebook</label>
                  <input
                    id="ebook"
                    placeholder=""
                    className="form-control-sm mx-2"
                    checked={book.isEbook}
                    name="isEbook"
                    type="checkbox"
                    onChange={onChangeEBook}
                  />
                </div>
              </div>
              <div>
                <div className="w-25 text-center">
                  <button className="btn btn-primary" type="submit">
                    <i className="fas fa-user-plus"></i>
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div className="w-25 p-2 text-center">
          <h4 className="text-uppercase mb-5">...Find it by USER ID...</h4>
          <form className="form-group">
            <div className="d-flex flex-column align-items-center justify-content-around">
              <div className="mb-2 d-flex align-items-center justify-content-between">
                <input
                  id="id"
                  placeholder="start typing id..."
                  className="form-control-sm mx-2"
                  value={book.idQuery}
                  name="idQuery"
                  type="text"
                  onChange={getBookByUserId}
                />
              </div>
            </div>
          </form>
        </div>
        <div className="w-25 p-2 text-center">
          <h4 className="text-uppercase mb-5">...or by its author and/or title...</h4>
          <form className="form-group">
            <div className="d-flex flex-column align-items-center justify-content-around">
              <div className="mb-2 d-flex align-items-center justify-content-between">
                <input
                  id="id"
                  placeholder="start typing title or author..."
                  className="form-control-sm mx-2"
                  value={book.freeQuery}
                  name="freeQuery"
                  type="text"
                  onChange={getBookByAuthorNameOrTitle}
                />
              </div>
            </div>
          </form>
        </div>
      </div>

      <div className="m-5 p-4 bg-primary bg-opacity-10 border border-dark text-center">
        <h3 className="text-uppercase mb-5">...and here's your list</h3>
        <div>
          <div className="d-flex justify-content-center mb-2">
            <div className="d-flex justify-content-between align-items-center w-75">
              <h6 className="text-uppercase fw-bold w-14-28">Id</h6>
              <h6 className="text-uppercase fw-bold w-14-28">User Id</h6>
              <h6 className="text-uppercase fw-bold w-14-28">Name</h6>
              <h6 className="text-uppercase fw-bold w-14-28">Author</h6>
              <h6 className="text-uppercase fw-bold w-14-28">Ebook available</h6>
              <h6 className="text-uppercase fw-bold w-14-28">Price</h6>
              <h6 className="text-uppercase fw-bold w-14-28">actions</h6>
            </div>
          </div>
          <ul className="list-unstyled">
            {listBooks.map((book) => (
              <li key={book._id} className="d-flex justify-content-center mb-2">
                <div className="d-flex justify-content-between align-items-center w-75">
                  <small className="w-14-28">{book._id}</small>
                  <small className="w-14-28">{book.userId}</small>
                  <small className="w-14-28">{book.name}</small>
                  <small className="w-14-28">{book.author}</small>
                  <small className="w-14-28">{book.isEbook ? <i className="fas fa-check"></i> : <i
                    className="fas fa-times"></i>}</small>
                  <small className="w-14-28">{book.price} €</small>
                  <div className="w-14-28">
                    <button
                      className="btn btn-danger"
                      onClick={(e) => deleteBook(book._id)} type="button">
                      <i className="fas fa-trash-alt"></i>
                    </button>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  )
}
